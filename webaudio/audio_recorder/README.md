# Audio Recorder

A simple, responsive, progressive web application that records audio clips using the
device's microphone.

The user interface is built with [Material Components Web](https://material.io/develop/web).
The audio is recorded with the [MediaStream Recording API](https://developer.mozilla.org/en-US/docs/Web/API/MediaStream_Recording_API)
and visualized with an [AnalyserNode](https://developer.mozilla.org/en-US/docs/Web/API/AnalyserNode).

## Installing

This web application has no dependencies. A Makefile is included for convenience.
