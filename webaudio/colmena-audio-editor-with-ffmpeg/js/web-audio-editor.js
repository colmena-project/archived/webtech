var userMediaStream;
var playlist;
var constraints = { audio: true };

navigator.getUserMedia = (navigator.getUserMedia ||
  navigator.webkitGetUserMedia ||
  navigator.mozGetUserMedia ||
  navigator.msGetUserMedia);

function gotStream(stream) {
  userMediaStream = stream;
  playlist.initRecorder(userMediaStream);
  $(".btn-record").removeClass("disabled");
}

function logError(err) {
  console.error(err);
}

playlist = WaveformPlaylist.init({
  samplesPerPixel: 3000,
  waveHeight: 100,
  container: document.getElementById("playlist"),
  state: 'cursor',
  colors: {
    waveOutlineColor: '#E0EFF1',
    timeColor: 'grey',
    fadeColor: 'black'
  },
  timescale: true,
  controls: {
    show: true, //whether or not to include the track controls
    width: 200 //width of controls in pixels
  },
  seekStyle : 'line',
  zoomLevels: [500, 1000, 3000, 5000]
});

//initialize the WAV exporter.
playlist.initExporter();

var elm = document.getElementById('uploader');
elm.addEventListener('change', add_new_track);

// add function to upload audio
function add_new_track( ) {
    for (var i = 0; i < elm.files.length; i++) {
        ee.emit("newtrack",  elm.files[0]);
    }
}

if (navigator.mediaDevices) {
    navigator.mediaDevices.getUserMedia(constraints)
        .then(gotStream)
        .catch(logError);
} else if (navigator.getUserMedia && 'MediaRecorder' in window) {
    navigator.getUserMedia(
        constraints,
        gotStream,
        logError
    );
};
