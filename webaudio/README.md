# WebAudio based audio recording and editing resources

Information about Web Audio W3C API for audio I/O and manipulation

# Standard references

- Web Audio API, W3C Recommendation, 17 June 2021: https://www.w3.org/TR/webaudio/

# Code references

- A complete multi-track audio editor and recorder (selected for use in Colmena!):
https://github.com/naomiaro/waveform-playlist

- A fork of waveform-playlist with support for mobile / touch interaction:
https://github.com/joshuabenker/ba-audio-edit

- Best WebAudio samples, which include a recorder:
https://github.com/GoogleChromeLabs/web-audio-samples

- A javascript library to encode the output of Web Audio API nodes in Ogg
  Opus or WAV format using WebAssembly:
https://github.com/chris-rudmin/opus-recorder

- A simple and useful single-track recorder and editor:
https://github.com/pkalogiros/AudioMass/

- A HTML5 WebAudio based Digital Audio Workstation:
https://github.com/gridsound/daw

- Audio Cat (Beautiful Audio Editor): https://github.com/google/beautiful-audio-editor

- A simple PWA audio recorder using Web Audio API:
https://github.com/mogwai/pwa-audio-recorder

- Very complete web audio test suite:
https://github.com/kaliatech/web-audio-recording-tests

- A simple and well implemented WebAudio recorder library:
https://github.com/2fps/recorder

- WebAudio Audio Peak Meter (VU meter):
https://github.com/esonderegger/web-audio-peak-meter

# WebAudio Streaming Solutions

- A protocol for audio streaming using the WebSocket API:
https://github.com/webcast/webcast.js

- A fully functional client for webcast.js:
https://github.com/webcast/webcaster

- Liquidsoup can consume webcast.js stream and send to icecast2:
https://github.com/savonet/liquidsoap

- Another project which uses the MediaRecorder and WebSocket API for streaming:
https://github.com/MuxLabs/wocket

- A simple but very useful recorder which call ffmpeg on the
  captured media:
https://github.com/chenxiaoqino/getusermedia-to-rtmp

- webcaster.js to icecast implementation, incluing the icecast protocol
  implementation in javascript:
https://github.com/social-dist0rtion-protocol/wasm-stream

 - MediaRecorder to HLS or DASH using modified ffmpeg.js:
https://github.com/davedoesdev/streamana

# Code Samples

- Very good sample references for simple audio recording:
https://blog.addpipe.com/recording-audio-in-the-browser-using-pure-html5-and-minimal-javascript/

- Directory "audio_recorder" contains a working audio recorder based on:
  https://github.com/GoogleChromeLabs/web-audio-samples

- Directory "audio_recoder2"  contains a working audio recorder with VU
  meter based on:
  https://github.com/kaliatech/web-audio-recording-tests-simpler

- Directory "waveform-playlist/" contains some waveform-playlist samples
  from:
  https://github.com/naomiaro/waveform-playlist

- Directory "colmena-audio-editor/" contains the example to be integrated to
  Colmena app, based on "web-audio-editor.js" waveform-playlist sample.

- Our Audio Cat (Beautiful Audio Editor) fork is at:
  https://github.com/ColmenaDev/beautiful-audio-editor

# Live tests

- audio_recorder: https://abradig.org.br/audio_recorder/

- audio_recorder2: https://abradig.org.br/audio_recorder2/

- Audio Cat: https://abradig.org.br/audiocat/
