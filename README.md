# Colmena Web W3C Technologies review

Information about Web Audio and Web Assembly W3C API for audio I/O and manipulation


# Contents

- Web Audio information: https://git.colmena.network/rafael2k/webtech/-/tree/main/webaudio

- Web Assembly information: https://git.colmena.network/rafael2k/webtech/-/tree/main/webassembly

- Streaming information:
https://git.colmena.network/rafael2k/webtech/-/tree/streaming


