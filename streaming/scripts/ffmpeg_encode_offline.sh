#!/bin/bash

#
# Copyright (C) 2021 Rafael Diniz <rafael@riseup.net>
#
# This is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this software; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street,
# Boston, MA 02110-1301, USA.
#

AUDIO_IN=${1}
OUTPUT_DIRECTORY=${2}

if [ -z "${AUDIO_IN}" ]
then
  echo "No argument supplied."
  echo "Usage: ${0} audio_file_name.ext [output_directory]"
  echo ""
  exit 0
fi

if [ -z "${OUTPUT_DIRECTORY}" ]
then
  echo "No output directory specified. Writing to local directory."
  OUTPUT_DIRECTORY=$(basename -- "${AUDIO_IN}")
fi

#filename=$(basename -- "${AUDIO_IN}")
#extension="${filename##*.}"
#filename="${filename%.*}"

mkdir -p ${OUTPUT_DIRECTORY}

ffmpeg -i ${AUDIO_IN} -y -map 0  -map -0:v \
    -codec:a libopus -codec:v none \
    -map a:0 -b:a:0 16k -ac:a:0 1 -ar:a:0 24000 \
    -map a:0 -b:a:1 24k -ac:a:1 1 -ar:a:1 48000 \
    -map a:0 -b:a:2 48k -ac:a:2 2 -ar:a:2 48000 \
    -map a:0 -b:a:3 64k -ac:a:3 2 -ar:a:3 48000 \
    -map a:0 -b:a:4 96k -ac:a:4 2 -ar:a:4 48000 \
    -map a:0 -b:a:5 128k -ac:a:5 2 -ar:5 48000 \
    -init_seg_name init\$RepresentationID\$.\$ext\$ -media_seg_name chunk\$RepresentationID\$-\$Number%05d\$.\$ext\$ \
    -use_template 1 -use_timeline 1  \
    -seg_duration 4 -adaptation_sets "id=0,streams=a" \
    -f dash ${OUTPUT_DIRECTORY}/dash.mpd

# to add - loudness normalization example...
# ffmpeg -i input -filter:a loudnorm  output

# for live, there is the following:
# FFMPEG DASH PARAMETERS

#. streaming streaming
#    Enable (1) or disable (0) chunk streaming mode of output. In chunk streaming mode, each frame will be a moof fragment which forms a chunk.
#. utc_timing_url utc_url
#    URL of the page that will return the UTC timestamp in ISO format. Example: "https://time.akamai.com/?iso" 
#. streaming streaming
#    Enable (1) or disable (0) chunk streaming mode of output. In chunk streaming mode, each frame will be a moof fragment which forms a chunk. 
#. window_size size
#    Set the maximum number of segments kept in the manifest.
#. extra_window_size size
#    Set the maximum number of segments kept outside of the manifest before removing from disk. 
#. remove_at_exit remove
#    Enable (1) or disable (0) removal of all segments when finished.
#. index_correction index_correction
#    Enable (1) or Disable (0) segment index correction logic. Applicable only when use_template is enabled and use_timeline is disabled.
#    When enabled, the logic monitors the flow of segment indexes. If a streams’s segment index value is not at the expected real time position, then the logic corrects that index value.
#    Typically this logic is needed in live streaming use cases. The network bandwidth fluctuations are common during long run streaming. Each fluctuation can cause the segment indexes fall behind the expected real time position. 
#. format_options options_list
#    Set container format (mp4/webm) options using a : separated list of key=value parameters. Values containing : special characters must be escaped.
#. global_sidx global_sidx
#    Write global SIDX atom. Applicable only for single file, mp4 output, non-streaming mode.
#. dash_segment_type dash_segment_type
#    Possible values:
#    auto
#        If this flag is set, the dash segment files format will be selected based on the stream codec. This is the default mode.
#    mp4
#        If this flag is set, the dash segment files will be in in ISOBMFF format.
#    webm
#        If this flag is set, the dash segment files will be in in WebM format. 
#. ldash ldash
#    Enable Low-latency Dash by constraining the presence and values of some elements.
#. update_period update_period
#    Set the mpd update period ,for dynamic content. The unit is second.



# for HLS
# VIDEO_OUT=master
# HLS_TIME=4

# HLS
#ffmpeg -i $VIDEO_IN -y \
#    -preset $PRESET_P -keyint_min $GOP_SIZE -g $GOP_SIZE -sc_threshold 0 -r $FPS -c:v libx264 -pix_fmt yuv420p \
#    -map v:0 -s:0 $V_SIZE_1 -b:v:0 2M -maxrate:0 2.14M -bufsize:0 3.5M \
#    -map v:0 -s:1 $V_SIZE_2 -b:v:1 145k -maxrate:1 155k -bufsize:1 220k \
#    -map v:0 -s:2 $V_SIZE_3 -b:v:2 365k -maxrate:2 390k -bufsize:2 640k \
#    -map v:0 -s:3 $V_SIZE_4 -b:v:3 730k -maxrate:3 781k -bufsize:3 1278k \
#    -map v:0 -s:4 $V_SIZE_4 -b:v:4 1.1M -maxrate:4 1.17M -bufsize:4 2M \
#    -map v:0 -s:5 $V_SIZE_5 -b:v:5 3M -maxrate:5 3.21M -bufsize:5 5.5M \
#    -map v:0 -s:6 $V_SIZE_5 -b:v:6 4.5M -maxrate:6 4.8M -bufsize:6 8M \
#    -map v:0 -s:7 $V_SIZE_6 -b:v:7 6M -maxrate:7 6.42M -bufsize:7 11M \
#    -map v:0 -s:8 $V_SIZE_6 -b:v:8 7.8M -maxrate:8 8.3M -bufsize:8 14M \
#    -map a:0 -map a:0 -map a:0 -map a:0 -map a:0 -map a:0 -map a:0 -map a:0 -map a:0 -c:a aac -b:a 128k -ac 1 -ar 44100\
#    -f hls -hls_time $HLS_TIME -hls_playlist_type vod -hls_flags independent_segments \
#    -master_pl_name $VIDEO_OUT.m3u8 \
#    -hls_segment_filename HLS/stream_%v/s%06d.ts \
#    -strftime_mkdir 1 \
#    -var_stream_map "v:0,a:0 v:1,a:1 v:2,a:2 v:3,a:3 v:4,a:4 v:5,a:5 v:6,a:6 v:7,a:7 v:8,a:8" HLS/stream_%v.m3u8


# Fallback video file
# ffmpeg -i $VIDEO_IN -y -c:v libx264 -pix_fmt yuv420p -r $FPS -s $V_SIZE_1 -b:v 1.8M -c:a aac -b:a 128k -ac 1 -ar 44100 fallback-video-$V_SIZE_1.mp4
