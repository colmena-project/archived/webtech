#!/bin/sh

echo "sample to test client / server streaming solution"

# on the server (MAIA Server)
ffmpeg -i tcp://127.0.0.1:8080?listen out.webm

#on the client (Community Broadcaster)
ffmpeg -re -f lavfi -i aevalsrc="sin(400*2*PI*t)" -c:a libopus -f webm tcp://127.0.0.1:8080

# TODO: Use rtp or other connection-less protocol?
