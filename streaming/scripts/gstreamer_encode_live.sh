#!/bin/sh

#
# Copyright (C) 2021 Rafael Diniz <rafael@riseup.net>
#
# This is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this software; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street,
# Boston, MA 02110-1301, USA.
#


# mpd-root-path

AUDIO_IN=${1}
OUTPUT_DIRECTORY=${2}

mkdir -p ${OUTPUT_DIRECTORY}

if [ "${AUDIO_IN}" != "live" ]
then
  echo "Only \"live\" input is supported this time."
  echo "Usage: ${0} live [output_directory]"
  echo ""
  exit 0
fi

if [ -z "${OUTPUT_DIRECTORY}" ]
then
  echo "No output directory specified. Writing to local directory."
  OUTPUT_DIRECTORY=$(basename -- "${AUDIO_IN}")
fi

# gst-launch-1.0 dashsink name=dashsink max-files=5  audiotestsrc is-live=true ! avenc_aac ! dashsink.audio_0 videotestsrc is-live=true ! x264enc ! dashsink.video_0

gst-launch-1.0 -m dashsink name=dashsink mpd-root-path=${OUTPUT_DIRECTORY} mpd-filename=dash.mpd target-duration=4 dynamic=false muxer=ts  audiotestsrc is-live=true ! opusenc ! dashsink.audio_0

# gst-launch-1.0 -m dashsink name=dashsink mpd-root-path=${OUTPUT_DIRECTORY} mpd-filename=dash.mpd target-duration=4 dynamic=false muxer=mp4  v4l2src ! video/x-raw,framerate=30/1,width=320,height=240 ! videoconvert ! queue ! x264enc bitrate=400 ! dashsink.video_0

# gst-launch-1.0 -m dashsink name=dashsink mpd-root-path=${OUTPUT_DIRECTORY} target-duration=4 dynamic=false period-duration=60000 muxer=ts  v4l2src ! video/x-raw,framerate=30/1,width=320,height=240 ! videoconvert ! queue ! x264enc bitrate=400 ! dashsink.video_0

# -m  Output messages posted on the pipeline's bus
# dashsink muxer: ts or mp4
