# streaming

This repository contains code for Maia's streaming platform.

## Standards

### DASH Standard

- ISO/IEC 23009-1:2019, Information technology — Dynamic adaptive streaming
  over HTTP (DASH) — Part 1: Media presentation description and segment
  formats: https://www.iso.org/standard/79329.html

### Bitstream Standards

- MPEG-2 TS Byte Stream Format, W3C Working Group Note, 04 October 2016:
  https://www.w3.org/TR/2016/NOTE-mse-byte-stream-format-mp2t-20161004/

- ISO/IEC 13818-1:2019, Information technology — Generic coding of moving
  pictures and associated audio information — Part 1: Systems:
  https://www.iso.org/standard/75928.html

- WebM Byte Stream Format, W3C Working Group Note, 04 October 2016:
  https://www.w3.org/TR/2016/NOTE-mse-byte-stream-format-webm-20161004/

## References

-  Simple GStreamer DASH example:
   https://www.collabora.com/news-and-blog/blog/2020/06/12/generating-mpeg-dash-streams-for-open-source-adaptive-streaming-with-gstreamer/

- Simple webpage with MIME-type and other muxing parameters for WebM:
  http://wiki.webmproject.org/adaptive-streaming/webm-dash-specification


- Collabora discussion about adaptive streaming:
  https://www.collabora.com/news-and-blog/blog/2020/06/12/generating-mpeg-dash-streams-for-open-source-adaptive-streaming-with-gstreamer/

- Very nice text covering DASH and HLS streaming with FFMPEG:
  https://blog.zazu.berlin/internet-programmierung/mpeg-dash-and-hls-adaptive-bitrate-streaming-with-ffmpeg.html

- DASH with WebM (with useful ffmpeg examples):
  https://developer.mozilla.org/en-US/docs/Web/Media/DASH_Adaptive_Streaming_for_HTML_5_Video

- WebRTC streaming using JANUS streaming plugin:
https://janus.conf.meetecho.com/docs/streaming.html

## Code references

- ISO/IEC DASH reference implementation: https://github.com/bitmovin/libdash

- DASH javascript-based player using W3C Media Source Extensions API -
  reference implementation by DASH Industry Forum:
  https://github.com/Dash-Industry-Forum/dash.js/wiki

- Gstreamer DASH implementation:
  https://gstreamer.freedesktop.org/documentation/dash/dashsink.html


## Authors

Rafael Diniz (rafael@riseup.net)

## License

This code is licensed under The GNU General Public License v3.0, or any
higher version.
