#
# Copyright (C) 2021 Rafael Diniz <rafael@riseup.net>
#
# This is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this software; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street,
# Boston, MA 02110-1301, USA.
#

#PREFIX=/usr
PREFIX=/var/www/html

# Set the compiler
# CC=gcc
# CPP=g++
CC=emcc
CPP=em++

# Flags
CFLAGS=-g -Wall
CXXFLAGS=
LDFLAGS=--shell-file shell_minimal.html

## Use this to target plain Java Script (slow!)
# LDFLAGS=--shell-file shell_minimal.html -s WASM=0

## output extension
# OUTPUT_TYPE=wasm
# OUTPUT_TYPE=mjs
# OUTPUT_TYPE=js
OUTPUT_TYPE=html


all: sample1.$(OUTPUT_TYPE)

sample1.$(OUTPUT_TYPE): sample1.o
	$(CC)  $(LDFLAGS) sample1.o -o sample1.$(OUTPUT_TYPE)

sample1.o: sample1.c
	$(CC) -c $(CFLAGS) sample1.c -o sample1.o


install: sample1.$(OUTPUT_TYPE)
	install -d $(PREFIX)/
	install sample1.html sample1.wasm sample1.js $(PREFIX)/

.PHONY: clean
clean:
	rm -f sample1.html  sample1.wasm  sample1.mjs  sample1.js sample1.o
