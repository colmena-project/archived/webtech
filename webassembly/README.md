# WebAssembly

WebAssembly repository for C and C++ experiments with WebAssembly

## Standard

### Core specification

- WebAssembly Core Specification, Editor’s Draft, 1 June 2021:
  https://webassembly.github.io/spec/core/bikeshed/

### Embedder specifications

- WebAssembly JavaScript Interface, Editor’s Draft, 1 June 2021:
https://webassembly.github.io/spec/js-api/index.html

- WebAssembly Web API, Editor’s Draft, 1 June 2021:
https://webassembly.github.io/spec/web-api/index.html

## C / C++ code development to WebAssembly

- Emscripten toolchain, based on LLVM and other tools:
https://emscripten.org/

- Emscripten toolchain is available on Debian 11 and Ubuntu 21.04 (and
  onward)

- WebAssembly references to Opus and Speex:
https://github.com/chris-rudmin/opus-recorder

### FFMPEG WebAssembly

- Core (C/C++ code) code to be compiled with Emscripten:
https://github.com/ffmpegwasm/ffmpeg.wasm-core

- FFMPEG.WASM javascript side:
https://github.com/ffmpegwasm/ffmpeg.wasm

- Needs Shared Memory in Web API:
https://hacks.mozilla.org/2020/07/safely-reviving-shared-memory/

- A streaming example using ffmpeg.wasm:
https://github.com/ColinEberhardt/ffmpeg-wasm-streaming-video-player

- More information about streaming using ffmpeg.wasm:
https://blog.scottlogic.com/2020/11/23/ffmpeg-webassembly.html

- ffmpeg with HLS and DASH support:
https://github.com/davedoesdev/ffmpeg.js

### Code samples

Code samples are available in directories samples*/

### Compiling

- First install Emscripten:
```
    apt-get install emscripten
```

- Go to sample directory and type (edit the Makefile to adapt to your system):
```
    make
```

### Samples

- Sample1: Hello world (to the terminal)

- Sample2: Draws white noise in a canvas window

- Sample3: Audio playback example

- Sample4: Audio recorder based on Chris Rudmin's code

- Sample5: FFmpeg samples

- Sample6: ffmpeg-wasm-streaming-video-player code

## Authors

Rafael Diniz (rafael@riseup.net)

## License

This code is licensed under The GNU General Public License v3.0, or any
higher version.
